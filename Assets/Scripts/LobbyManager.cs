﻿using System;
using SWNetwork;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using UnityEditor;

public class LobbyManager : MonoBehaviour
{
    public Button registerButton;
    public Button playButton;
    public TMP_InputField customField;

    [SerializeField]
    private int gameScene;
    [SerializeField]
    private int minPlayers = 4;
    [SerializeField]
    private TextMeshProUGUI userUI;

    private string username;

    // Start is called before the first frame update
    void Start()
    {
        NetworkClient.Lobby.OnRoomReadyEvent += Lobby_OnRoomReadyEvent;
        NetworkClient.Lobby.OnFailedToStartRoomEvent += Lobby_OnFailedToStartRoomEvent;
        NetworkClient.Lobby.OnLobbyConnectedEvent += Lobby_OnLobbyConnectedEvent;

        username = MainMenuManager.username_string;
        userUI.text = username;
        customField.text = username;

        registerButton.gameObject.SetActive(true);
        playButton.gameObject.SetActive(false);
    }

    private void OnDestroy()
    {
        NetworkClient.Lobby.OnRoomReadyEvent -= Lobby_OnRoomReadyEvent;
        NetworkClient.Lobby.OnFailedToStartRoomEvent -= Lobby_OnFailedToStartRoomEvent;
        NetworkClient.Lobby.OnLobbyConnectedEvent -= Lobby_OnLobbyConnectedEvent;
    }


    public void BackToMenu()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }

    void Lobby_OnRoomReadyEvent(SWRoomReadyEventData eventData) {
        Debug.Log("Room is ready : roomID = " + eventData.roomId);
        ConnectToRoom();
    }

    void Lobby_OnFailedToStartRoomEvent(SWFailedToStartRoomEventData eventData) {
        Debug.Log("Failed to start room : " + eventData);
    }

    void Lobby_OnLobbyConnectedEvent() {
        Debug.Log("Lobby connected");
        RegisterPlayer();
    }

    public void Register() {
        string customPlayerID = customField.text;

        if (customPlayerID != null && customPlayerID.Length > 0)
        {
            NetworkClient.Instance.CheckIn(customPlayerID, (bool ok, string error) =>
            {
                if (!ok) {
                    Debug.LogError("Check failed : " + error);
                }
            });
        }

    }

    public void Play() {
        NetworkClient.Lobby.JoinOrCreateRoom(true, 2, 60, HandleJoinOrCreatedRoom);
    }

    void RegisterPlayer() {
        NetworkClient.Lobby.Register((succesful, reply, error) =>
        {
            if (succesful) {
                Debug.Log("Registered " + reply);

                if (reply.started) {
                    ConnectToRoom();
                }
                else {
                    playButton.gameObject.SetActive(true);
                    registerButton.gameObject.SetActive(false);
                }

            }
        });
    }

    void HandleJoinOrCreatedRoom(bool succesful, SWJoinRoomReply reply, SWLobbyError error) {
        if (succesful) {
            Debug.Log("Joined or created room " + reply);

            if (reply.started) {
                ConnectToRoom();
            } else if (NetworkClient.Lobby.IsOwner){
                StartRoom();
            }

        }
        else {
            Debug.Log("Failed to join or create room " + error);
        }
    }

    void StartRoom() {
        NetworkClient.Lobby.StartRoom((okay, error) => {
            if (okay) {
                Debug.Log("Started room");
            }
            else {
                Debug.Log("Failed to start room " + error);
            }
        });
    }

    void ConnectToRoom() {
        NetworkClient.Instance.ConnectToRoom(HandleConnectedToRoom);
    }

    void HandleConnectedToRoom(bool connected) {
        if (connected) {
            Debug.Log("Connected to room.");
            SceneManager.LoadScene(gameScene);
        }
        else {
            Debug.Log("Failed to connect to room.");
        }
    }
}
