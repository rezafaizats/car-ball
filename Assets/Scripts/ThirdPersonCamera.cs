﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonCamera : MonoBehaviour
{
    public bool isFollowing;
    public float camSensitivity = 10;
    public GameObject target;
    public float distFromTarget = 2.75f;
    public Vector2 pitchMinMax = new Vector2(-35, 85);

    public float rotSmoothTime = .12f;
    Vector3 rotSmoothVelocity;
    Vector3 currentRot;

    float yaw;
    float pitch;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void LateUpdate()
    {
        yaw += Input.GetAxis("Mouse X");
        pitch -= Input.GetAxis("Mouse Y");
        pitch = Mathf.Clamp(pitch, pitchMinMax.x, pitchMinMax.y);

        if (target != null)
        {
            //Activate smooth
            currentRot = Vector3.SmoothDamp(currentRot, new Vector3(pitch, yaw), ref rotSmoothVelocity, rotSmoothTime);

            //Vector3 targetRot = new Vector3(pitch, yaw);
            transform.eulerAngles = currentRot;

            transform.position = target.transform.position - transform.forward * distFromTarget;
        }
    }
}
