﻿using SWNetwork;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum Axel {
    Front,
    Rear,
}

[Serializable]
public struct Wheel {
    public GameObject model;
    public WheelCollider wheelCollider;
    public Axel axel;
}

public class CarController : MonoBehaviour
{
    [HideInInspector]
    public Vector2 MoveAxis;

    [SerializeField]
    private FixedJoystick moveJoystick;
    [SerializeField]
    private GameObject uiCanvas;
    [SerializeField]
    private float maxAccel = 20.0f;
    [SerializeField]
    private float turnSensitivity = 1.0f;
    [SerializeField]
    private float maxSteerAngle = 45.0f;
    [SerializeField]
    private Vector3 pivot;
    [SerializeField]
    private float forcePower = 5f;
    [SerializeField]
    private List<Wheel> wheels;

    private NetworkID networkID;
    private float inputX, inputY;
    private Rigidbody _rb;
    private bool isCameraLocking = false;

    void Start()
    {
        _rb = GetComponent<Rigidbody>();
        networkID = GetComponent<NetworkID>();

        _rb.centerOfMass = pivot;

        if (networkID.IsMine) {
            Camera m_mainCamera = Camera.main;
            ThirdPersonCamera cameraFollow = m_mainCamera.GetComponent<ThirdPersonCamera>();
            cameraFollow.target = this.gameObject;

            uiCanvas.SetActive(true);
        }
    }

    void Update()
    {
        if (networkID.IsMine) {
            AnimateWheels();
            GetInputs();
        }
    }

    private void LateUpdate()
    {
        if (networkID.IsMine) {
            Move();
            Turn();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (networkID.IsMine)
        {
            if (collision.gameObject.tag == "Balls")
            {
                collision.gameObject.GetComponent<BallController>().ForceBall(collision.GetContact(0).normal);
            }
        }
    }

    private void GetInputs()
    {
        //inputX = Input.GetAxis("Horizontal");
        //inputY = Input.GetAxis("Vertical");

        inputX = moveJoystick.Horizontal;
        inputY = moveJoystick.Vertical;

        if (Input.GetKeyUp(KeyCode.Space))
        {
            Vector3 _power = Vector3.up * 5f;

            _rb.AddForce(_power, ForceMode.VelocityChange);
        }

        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            Vector3 _power = Vector3.forward * 5f;

            _rb.AddRelativeForce(_power, ForceMode.VelocityChange);
        }

    }

    private void AnimateWheels()
    {
        foreach (var wheel in wheels)
        {
            Quaternion _rot;
            Vector3 _pos;
            wheel.wheelCollider.GetWorldPose(out _pos, out _rot);
            wheel.model.transform.position = _pos;
            wheel.model.transform.rotation = _rot;
        }
    }

    private void Move()
    {
        foreach (var wheel in wheels)
        {
            wheel.wheelCollider.motorTorque = inputY * maxAccel * 500 * Time.deltaTime;
        }
    }

    public void Boost()
    {
        Vector3 _power = Vector3.forward * 5f;
        _rb.AddRelativeForce(_power, ForceMode.VelocityChange);
    }

    public void Jump()
    {
        Vector3 _power = Vector3.up * 5f;
        _rb.AddForce(_power, ForceMode.VelocityChange);
    }

    private void Turn() {
        foreach (var wheel in wheels)
        {
            if (wheel.axel == Axel.Front)
            {
                var _steerAngle = inputX * turnSensitivity * maxSteerAngle;
                wheel.wheelCollider.steerAngle = Mathf.Lerp(wheel.wheelCollider.steerAngle, _steerAngle, 0.5f);
            }
        }
    }

    public void exitGame() {
        NetworkClient.Instance.DisconnectFromRoom();
        NetworkClient.Lobby.LeaveRoom(HandleLeaveRoom);
    }

    void HandleLeaveRoom(bool okay, SWLobbyError error) {
        if (!okay)
        {
            Debug.LogError(error);
        }

        Debug.Log("Left room.");
        SceneManager.LoadScene("Main Menu");
    }

}
