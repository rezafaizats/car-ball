﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using SWNetwork;
using System;
using TMPro;

public class GameController : MonoBehaviour
{
    private int Team1Score, Team2Score;
    private int spawnPointIndex = 0;
    private NetworkID networkID;
    private RoomPropertyAgent roomAgent;

    [SerializeField]
    private GameObject Ball;
    [SerializeField]
    private Text ScoreNow;
    [SerializeField]
    private GameObject GameOverPanel;
    [SerializeField]
    private TextMeshProUGUI scoreTextEnd;

    [Serializable]
    public class Score
    {
        public int Team1Score;
        public int Team2Score;
    }

    // Start is called before the first frame update
    void Start()
    {
        networkID = GetComponent<NetworkID>();
        roomAgent = GetComponent<RoomPropertyAgent>();

        Team1Score = 0;
        Team2Score = 0;
    }

    public void AddTeamScore(int whichTeam)
    {
        Debug.Log("Goalie for team : " + whichTeam);
        ScoreNetworkUpdate(whichTeam);
    }

    public void UpdateScore(int val1, int val2)
    {
        ScoreNow.text = val1.ToString() + " - " + val2.ToString();
    }

    public void OnSpawnerReady(bool alreadySetup, SceneSpawner sceneSpawner) {
        Debug.Log("OnSpawnerReady : " + alreadySetup);

        if (!alreadySetup){
            spawnPointIndex = UnityEngine.Random.Range(0, 4);
            sceneSpawner.SpawnForPlayer(0, spawnPointIndex);

            sceneSpawner.PlayerFinishedSceneSetup();
            spawnPointIndex++;
            if (spawnPointIndex >= 3)
                spawnPointIndex = 0;
        }
    }

    public void ScoreNetworkUpdate(int WhichTeam) {

        SWSyncedProperty agent = roomAgent.GetPropertyWithName("gameScores");

        Score GameScore = null;

        if (agent != null)
        {
            GameScore = agent.GetValue<Score>();
        }

        //Score GameScore = roomAgent.GetPropertyWithName("gameScores").GetValue<Score>();

        if (GameScore == null)
        {
            GameScore = new Score();
        }

        bool foundGameScore = false;

        if (WhichTeam == 1) {
            GameScore.Team1Score++;
            foundGameScore = true;
        }
        else
        {
            GameScore.Team2Score++;
            foundGameScore = true;
        }

        ScoreNow.text = GameScore.Team1Score.ToString() + " - " + GameScore.Team2Score.ToString();
        roomAgent.Modify<Score>("gameScores", GameScore);
    }

    public void OnScoreChanged() {

        Score GameScore = roomAgent.GetPropertyWithName("gameScores").GetValue<Score>();

        Debug.Log(GameScore);

        if (GameScore != null)
        {
            if (GameScore.Team1Score >= 3 || GameScore.Team2Score >= 3)
            {
                GameOverPanel.SetActive(true);
                scoreTextEnd.text = GameScore.Team1Score + " - " + GameScore.Team2Score;
            }
        }
    }

    public void exitGame()
    {
        NetworkClient.Instance.DisconnectFromRoom();
        NetworkClient.Lobby.LeaveRoom(HandleLeaveRoom);
    }

    void HandleLeaveRoom(bool okay, SWLobbyError error)
    {
        if (!okay)
        {
            Debug.LogError(error);
        }

        Debug.Log("Left room.");
        SceneManager.LoadScene("Main Menu");
    }

}