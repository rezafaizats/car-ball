﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI username;

    public static string username_string = "Guest";

    private void Start()
    {
        int rand = Random.Range(1, 25000);
        username_string = "Guest" + rand.ToString();

        username.text = "Playing as : " + username_string;
    }

    public void PlayGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void QuitGame()
    {
        Debug.Log("QUIT");
        Application.Quit();
    }

    public void getUsernamePlayer(string user) {
        user = username_string;
        username.text = username_string;
    }

}
