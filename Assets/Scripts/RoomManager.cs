﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RoomManager : MonoBehaviour
{
    [Header("UI")]
    [SerializeField] private TMP_Text[] playerNameTexts = new TMP_Text[4];
    [SerializeField] private TMP_Text[] playerReadyText = new TMP_Text[4];
    [SerializeField] private Button startGameButton = null;

    //[SyncVar(hook = nameof(HandleDisplayNameChange))]
    //public string DisplayName = "Loading...";
    //[SyncVar(hook = nameof(HandleReadyStatusChange))]
    //public bool isReady = false;

    private bool isRoomHost = false;
    public bool IsLeader {
        set {
            isRoomHost = value;
        }
    }

    void Start()
    {

    }

    void Update()
    {

    }

    public void HandleReadyStatusChange(bool oldVal, bool newVal) => UpdateDisplay();
    public void HandleDisplayNameChange(string oldVal, string newVal) => UpdateDisplay();

    private void UpdateDisplay() {

    }

    public void HandleReadyToStart(bool readyToStart) {
        if (!isRoomHost) {
            return;
        }
        startGameButton.interactable = readyToStart;
    }

}
