﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.Multiplayer;

public class playAuth : MonoBehaviour
{
    public static PlayGamesPlatform platform;
    public Button login;
    public Button achievement;

    private string user;

    // Start is called before the first frame update
    void Start()
    {
        if (platform == null)
        {
            PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
                                                    .Build();
            PlayGamesPlatform.InitializeInstance(config);
            PlayGamesPlatform.DebugLogEnabled = true;

            platform = PlayGamesPlatform.Activate();
            login.gameObject.SetActive(true);
            achievement.gameObject.SetActive(false);
        }

        if (Social.localUser.userName != null) {
            user = Social.localUser.userName;
            MainMenuManager mainMenuManager = FindObjectOfType<MainMenuManager>();
            mainMenuManager.getUsernamePlayer(user);
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SignIn(){
        Social.localUser.Authenticate((bool success) =>{
            if(success)
            {
                Debug.Log("Succes login");
                loginAchie();
                login.gameObject.SetActive(false);
                user = Social.localUser.userName;
                MainMenuManager mainMenuManager = FindObjectOfType<MainMenuManager>();
                mainMenuManager.getUsernamePlayer(user);
            }
            else
            {
                Debug.Log("fail");
            }
        });
    }

    public void loginAchie()
    {
        Social.ReportProgress(GPGSIds.achievement_welcome, 100f, null);
    }

    public void ShowAchie()
    {
        if(PlayGamesPlatform.Instance.localUser.authenticated)
        {
            PlayGamesPlatform.Instance.ShowAchievementsUI();
        }
        else
        {
            Debug.Log("Cannot show achievement");
        }
    }
}
